package dns;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

public class Main {
	
	private static final String PORTA = "porta";
	private static final String IP_PAI = "ipPai";
	private static final String PORTA_PAI = "portaPai";
	
	public static void main(String[] args) throws IOException{
		
		validaArgumentos();
		
		HttpServer server = HttpServer.create(new InetSocketAddress(getPorta()), 0);
        server.createContext(RegistraServico.CONTEXTO, new RegistraServico());
        server.start();
		
	}

	private static int getPorta() {
		String porta = System.getProperty(PORTA);
		
		return Integer.valueOf(porta);
	}

	private static void validaArgumentos() {
		if(System.getProperty(PORTA) == null)
			throw new RuntimeException("Informe a porta com -Dporta.");
		
	}

}
