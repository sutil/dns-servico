package dns;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;

public class RegistraServico extends Servico {
	
	public static final String CONTEXTO = "/registra";
	private static final String NOME_SERVICO = "nome";
	private static final String IP = "ip";
	private static final String PORTA = "porta";
	
	String ip;
	String porta;
	String nome;

	@Override
	public String getContexto() {
		return "registra";
	}

	@Override
	void setParametro(String chave, String valor){
		
		if(NOME_SERVICO.equals(chave))
			this.nome = valor;
		
		
		else if(IP.equals(chave))
			this.ip = valor;
		
		else if(PORTA.equals(chave))
			this.porta = valor;
		
		else
			throw new RuntimeException("Parâmetro '"+chave+"' incosistente");
	}

	@Override
	int getQuantidadeParametros() {
		return 3;
	}
	
	@Override
	void executa() {
		Endereco endereco = new Endereco(this.ip, this.porta);
		TabelaRoteamento.add(this.nome, endereco);
	}

	@Override
	void addResponse(HttpExchange args) throws IOException {
		String response = "Serviço registrado";
        args.sendResponseHeaders(200, response.length());
        OutputStream os = args.getResponseBody();
        os.write(response.getBytes());
        os.close();
	}
	
}
